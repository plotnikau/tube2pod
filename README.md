# What is Tube2Pod Bot
This is my favourite (and really simple) telegram bot, which helps me to consume a lot of information in most convinient way, anytime, just by listening, not watching.  

You send a link to youtube videos to Tube2Pod Bot and it adds audio stream to your personal podcast feed.

Sounds interesting?

# How it works?
* Video is downloaded from youtube  
* Audio is extracted by ffmpeg  
* Audio is uploaded to archive.org  
* archive.org search RSS feed can be imported to podcast client of your choice  

# Can I try it right away without installing & compiling stuff?
Yes!   
You are welcome to start telegram chat with [tube2podBot](https://telegram.me/tube2podBot) and feed the bot with your youtube links.

# Want to dive deeper?
Let's build & run locally

## Basic preparations
Retrieve your API tokens:
* Create your telegram bot via [BotFather](https://telegram.me/BotFather) and note your telegram bot token
* Register a user for archive.org and retrieve your API key as described [here](https://archive.org/services/docs/api/ias3.html)
* If you decide to run bot locally (not in docker), install [ffmpeg](https://ffmpeg.org)
* Otherwise ```docker``` is the only requirement

## Build with ```golang``` and ```dep``` locally
Install ```golang``` and ```dep``` for dependencies management  

Clone repository under ```GOPATH``` (we'll use ```dep```, which wants it for some reason)  

Satisfy the dependencies and build:
```
dep ensure --vendor-only
go build -o tube2pod .
```

## Build in Docker locally 
This could be the most convinient way for those, who doesn't have any golang dev tools locally.  

Only docker is required to be installed locally, neither ```golang``` nor ```dep``` is required.  

Put your archive.org and telegram bot tokens in ```localBuildWithDocker.sh``` and run it.


# Feel free to contribute!
*even if tube2pod is doing what it should do almost perfect for me :)*
