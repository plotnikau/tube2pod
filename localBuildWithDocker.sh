#!/usr/bin/env bash
docker stop tube2pod

docker rm tube2pod

docker build -t tube2pod -f docker/Dockerfile.local .

docker run -d --env=ARCHIVE_AUTH_STRING="<REPLACE_WITH_YOUR_ARCHIVE_AUTH_STRING>" --env=TELEGRAM_BOT_TOKEN="<REPLACE_WITH_YOUR_TELEGRAM_BOT_TOKEN>" --name tube2pod tube2pod
